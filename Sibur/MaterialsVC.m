//
//  MaterialsVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/7/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "MaterialsVC.h"
#import "MainCollectionCell.h"
#import "TransfersVC.h"


@interface MaterialsVC ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation MaterialsVC{
    NSArray *docs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *docsTemp = [userDefaults objectForKey:@"docs"];
    docs = docsTemp;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return docs.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MainCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *doc = docs[indexPath.row];
    cell.label.text = doc[@"title"];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionView.frame.size.width/3 - 20, self.collectionView.frame.size.height/3 - 20);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSDictionary *doc = docs[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"doc%d.pdf",[doc[@"id"] intValue]];
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        TransfersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TransfersVC"];
        vc.navigationItem.title = doc[@"title"];
        vc.nameFile = name;
        [self.navigationController pushViewController:vc animated:YES];
    }
}



@end
