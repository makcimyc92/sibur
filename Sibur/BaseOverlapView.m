//
//  BaseOverlapView.m
//  Torless
//
//  Created by Max Vasilevsky on 8/18/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "BaseOverlapView.h"

@interface BaseOverlapView ()

@property (strong, nonatomic) UIView *backgroundView;

@end

@implementation BaseOverlapView
{
    OverlapViewBlock _blockCancel,  _blockApply;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self.buttonApply addTarget:self action:@selector(buttonApplyAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel addTarget:self action:@selector(buttonCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(dismiss)];
    [self.backgroundView addGestureRecognizer:tapBackground];
    self.backgroundColor = [UIColor clearColor];
}

#pragma mark - Lazy Init

-(UIView *)backgroundView{
    if (!_backgroundView) {
        if (!UIAccessibilityIsReduceTransparencyEnabled()) {
            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            _backgroundView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            _backgroundView.frame = self.bounds;
            _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        } else {
            _backgroundView = [UIView new];
            _backgroundView.frame = self.bounds;
            _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            _backgroundView.backgroundColor = [UIColor blackColor];
            _backgroundView.alpha = 0.3;
        }
        [self addSubview:_backgroundView];
        [self sendSubviewToBack:_backgroundView];
    }
    return _backgroundView;
}

#pragma mark - button actions

- (IBAction)buttonCancelAction:(id)sender{
    if (_blockCancel)
        _blockCancel(self);
    
    [self dismiss];
}

- (IBAction)buttonApplyAction:(id)sender{
    if (_blockApply)
        _blockApply(self);
    
    [self dismiss];
}

-(void) dismiss{
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView setTransform:CGAffineTransformMakeScale(0.000001f, 0.000001f)];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)showWithApplyBlock:(OverlapViewBlock)blockApply cancelBlock:(OverlapViewBlock)blockCancel{
    self.frame = [[[UIApplication sharedApplication] delegate] window].bounds;
    
    _blockApply = blockApply;
    _blockCancel = blockCancel;
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    
    [self.contentView setTransform:CGAffineTransformMakeScale(0, 0)];
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView setTransform:CGAffineTransformMakeScale(1, 1)];
        [self.contentView setAlpha:1];
    }];
    
}


-(void)showWithApplyBlock:(OverlapViewBlock)blockApply{
    [self showWithApplyBlock:blockApply cancelBlock:nil];
}


-(void)show{
    [self showWithApplyBlock:nil cancelBlock:nil];
}


#pragma mark - class methods


+(void)showWithApplyBlock:(OverlapViewBlock)blockApply cancelBlock:(OverlapViewBlock)blockCancel{
    BaseOverlapView *view = [[self class] makeFromXib];
    view.frame = [[[UIApplication sharedApplication] delegate] window].bounds;
    
    view->_blockApply = blockApply;
    view->_blockCancel = blockCancel;
    
    [[[[UIApplication sharedApplication] delegate] window].subviews.lastObject addSubview:view];
    
    [view.contentView setTransform:CGAffineTransformMakeScale(0, 0)];
    
    [UIView animateWithDuration:0.2 animations:^{
        [view.contentView setTransform:CGAffineTransformMakeScale(1, 1)];
        [view.contentView setAlpha:1];
    }];
}

+(void)showWithApplyBlock:(OverlapViewBlock)blockApply{
    [self showWithApplyBlock:blockApply cancelBlock:nil];
}

+(void)show{
    [self showWithApplyBlock:nil cancelBlock:nil];
}



@end
