//
//  CheckBoxCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/7/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "CheckBoxCell.h"



@implementation CheckBoxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapVariant:)];
    [self.contentView addGestureRecognizer:tap];
}

-(void)layoutSubviews{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)tapVariant:(UIButton*)sender{
    self.checkBox.highlighted = !self.checkBox.highlighted;
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
    NSString *key = [NSString stringWithFormat:@"%d",self.idAnswer];
    self.completeVote[key] = self.checkBox.highlighted ? @1 : @0;
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
//    NSString *key = [NSString stringWithFormat:@"%ld_%ld",indexPath.section+1,indexPath.row+1];
//    _votes[key] = self.checkBox.highlighted ? @1 : @0;
//    NSLog(@"%ld_%ld",indexPath.section+1,indexPath.row+1);
}

@end
