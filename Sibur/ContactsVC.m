//
//  ContactsVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/4/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "ContactsVC.h"
#import "ContactsCell.h"

@interface ContactsVC ()

@property (weak,nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ContactsVC{
    NSArray *contactsData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *contacts = [userDefaults objectForKey:@"contacts"];
    contactsData = contacts;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.view.backgroundColor = self.tableView.backgroundColor;
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 140;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return contactsData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"contactsCell";
    ContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"ContactsCell" bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    NSDictionary *contact = contactsData[indexPath.row];
    cell.nameLabel.text = contact[@"name"];
    cell.subLabel.text = contact[@"position"];
    [cell.subLabel sizeToFit];
    [cell.phone setTitle:contact[@"phone_no"] forState:UIControlStateNormal];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *name = [NSString stringWithFormat:@"contact%d",[contact[@"id"] intValue]];
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
        cell.fotoView.image = image;
    } else {
        cell.fotoView.image = nil;
    }
//    cell.companyLabel.text = self.companyName;
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}




@end
