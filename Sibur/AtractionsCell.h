//
//  AtractionsCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/5/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtractionsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
@property (weak, nonatomic) IBOutlet UILabel *sheduleLabel;
//@property (weak, nonatomic) IBOutlet UILabel *place;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImage;

@property (strong, nonatomic) NSString *phoneString;

@end
