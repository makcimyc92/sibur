//
//  PhotoVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 8/16/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "PhotoVC.h"

@interface PhotoVC ()

@property (weak, nonatomic) IBOutlet UIScrollView *imagesScroll;

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;

@end

@implementation PhotoVC{
    NSMutableArray *images;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    images = [NSMutableArray new];
    NSArray *photos = [[NSUserDefaults standardUserDefaults] objectForKey:@"photos"];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    for (NSDictionary *photo in photos) {
        NSString *url = photo[@"url"];
        NSString *name = [NSString stringWithFormat:@"photo%d.%@",[photo[@"id"] intValue],url.pathExtension];
        NSString *filePath = [basePath stringByAppendingPathComponent:name];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [images addObject:filePath];
        }
    }
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setImagesForImageScroll];
}


- (IBAction)tapLeftImage:(UITapGestureRecognizer*)sender {
    if (_imagesScroll.contentOffset.x > 0) {
        sender.view.userInteractionEnabled = NO;
        CGPoint newContentOffset = CGPointMake(_imagesScroll.contentOffset.x - CGRectGetWidth(_imagesScroll.frame),
                                               0);
        [UIView animateWithDuration:0.2 animations:^{
            _imagesScroll.contentOffset = newContentOffset;
        } completion:^(BOOL finished) {
            sender.view.userInteractionEnabled = YES;
        }];
    }
}
- (IBAction)tapRightImage:(UITapGestureRecognizer*)sender {
    if (_imagesScroll.contentOffset.x < _imagesScroll.contentSize.width - _imagesScroll.frame.size.width) {
        sender.view.userInteractionEnabled = NO;
        CGPoint newContentOffset = CGPointMake(_imagesScroll.contentOffset.x + CGRectGetWidth(_imagesScroll.frame),
                                               0);
        [UIView animateWithDuration:0.2 animations:^{
            _imagesScroll.contentOffset = newContentOffset;
        } completion:^(BOOL finished) {
            sender.view.userInteractionEnabled = YES;
        }];
    }
}

#pragma mark - func's

-(void)setImagesForImageScroll{
    if (images.count) {
        for (NSString *path in images) {
            UIImageView *imageView = [UIImageView new];
            imageView.frame = CGRectMake(CGRectGetWidth(_imagesScroll.frame) * [images indexOfObject:path],
                                         0,
                                         CGRectGetWidth(_imagesScroll.frame),
                                         CGRectGetHeight(_imagesScroll.frame));
            imageView.image = [UIImage imageWithContentsOfFile:path];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [_imagesScroll addSubview:imageView];
        }
        _imagesScroll.contentSize = CGSizeMake(CGRectGetWidth(_imagesScroll.frame) * images.count,
                                               0);
    } else {
//        _constraintImageScrollHeight.constant = 0;
        _leftImageView.hidden = _rightImageView.hidden = YES;
        return;
    }
    if (images.count == 1) {
        _leftImageView.alpha = _rightImageView.alpha = 0.1;
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self setOffsetFor:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self setOffsetFor:scrollView];
}


-(void)setOffsetFor:(UIScrollView*)scrollView{
    float index = scrollView.contentOffset.x / scrollView.frame.size.width;
    double integral;
    double fractional = modf(index, &integral);
    if (fractional  < 0.5) {
        CGPoint offset = CGPointMake(CGRectGetWidth(scrollView.frame) * integral, 0);
        [scrollView setContentOffset:offset animated:YES];
    } else {
        CGPoint offset = CGPointMake(CGRectGetWidth(scrollView.frame) * (integral + 1), 0);
        [scrollView setContentOffset:offset animated:YES];
    }
}


@end
