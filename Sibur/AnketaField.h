//
//  AnketaField.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/8/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnketaField : UIView

@property (weak, nonatomic) UILabel *title;
@property (weak, nonatomic) UITextField *textField;

@end
