//
//  VotesVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/7/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//


#import "VotesVC.h"
#import "CheckBoxCell.h"
#import "HeaderSectionVoteView.h"

@interface VotesVC ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation VotesVC{
    NSArray *votes;
//    NSMutableDictionary *currentVotes;
    NSMutableDictionary *completeVote;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView.frame = CGRectMake(0, 0, self.view.frame.size.width, 100);
    self.navigationItem.title = @"Голосование";
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *voteaTemp = [userDefaults objectForKey:@"votea"];
    voteaTemp = [voteaTemp sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return obj1[@"id"] > obj2[@"id"];
    }];
    NSArray *voteqTemp = [userDefaults objectForKey:@"voteq"];
    voteqTemp = [voteqTemp sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return obj1[@"id"] > obj2[@"id"];
    }];
    int count = -1;
    completeVote = [NSMutableDictionary new];
    NSMutableArray *votesTemp = [NSMutableArray new];
    for (NSDictionary *qustion in voteqTemp) {
        NSMutableArray *answers = [NSMutableArray new];
        for (NSDictionary *answer in voteaTemp) {
            if ([answer[@"quastion_id"] intValue] == [qustion[@"id"] intValue]) {
                count++;
                [answers addObject:answer];
                NSString *key = [NSString stringWithFormat:@"%d",[answer[@"id"] intValue]];
                completeVote[key] = @0;
            }
        }
        NSDictionary *vote = @{@"answers":answers,
                               @"q":qustion};
        [votesTemp addObject:vote];
    }
    votes = votesTemp;
//    [self createCompleteVoteForCount:count];
    if (votes.count == 0) {
        self.tableView.hidden = YES;
        UILabel *label = [UILabel new];
        label.text = @"Доступных голосований нет";
        [label sizeToFit];
        label.textColor = [UIColor grayColor];
        label.center = self.view.center;
        [self.view addSubview:label];
    }
}

#pragma mark - func's
//
//-(void)createCompleteVoteForCount:(int)count{
//    completeVote = [NSMutableDictionary new];
//    for (int i = 0 ; i <= count; i ++) {
//        [completeVote addObject:@0];
//    }
//}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return votes.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSDictionary *dictVote = votes[section];
    NSArray *answers = dictVote[@"answers"];
    return answers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"CheckBoxCell";
    CheckBoxCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"CheckBoxCell" bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    cell.tableView = self.tableView;
    cell.completeVote = completeVote;
    NSDictionary *dictVote = votes[indexPath.section];
    NSArray *answers = dictVote[@"answers"];
    cell.variantLabel.text = answers[indexPath.row][@"text"];
    cell.idAnswer = [answers[indexPath.row][@"id"] intValue];
    NSString *key = [NSString stringWithFormat:@"%d",cell.idAnswer];
    cell.checkBox.highlighted = [completeVote[key] intValue];
    return cell;
}

#pragma mark - UITableViewDelegate

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSectionVoteView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"HeaderSectionVoteView" owner:self options:nil] objectAtIndex:0];
    NSDictionary *vote = votes[section];
    NSDictionary *q = vote[@"q"];
    headerView.voteLabel.text = q[@"question"];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Action's

-(IBAction)tapSend:(UIButton*)sender{
    sender.enabled = NO;
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue:nil];
    
    NSURL * url = [NSURL URLWithString:@"http://siburtop.aboutthecode.ru/web/index.php?r=site%2Fpostvote"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    NSString *answerString = @"";
    NSArray *keys = [self sortedIDs:completeVote.allKeys];
    for (NSString *key in keys) {
        answerString = [NSString stringWithFormat:@"%@%d",answerString,[completeVote[key] intValue]];
    }
    NSString *login = [[NSUserDefaults standardUserDefaults] objectForKey:@"login"];
    NSString *params = [NSString stringWithFormat:@"user_id=%@&string=%@",login,answerString];
//    answerString = [answerString substringToIndex:answerString.length - 1];
//    params = [params substringToIndex:params.lrrrrrrrrrength - 1];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString *message;
        if (!error) {
            message = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            message = @"Спасибо предоставленная информация будет обработана!";
        } else {
            message = @"Ошибка отправления";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showMessageLabel:message];
//            [self showAlert:message];
        });
        sender.enabled = YES;
    }];
    
    [dataTask resume];
}

-(NSArray*)sortedIDs:(NSArray*)array{
    return [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 intValue]> [obj2 intValue];
    }];
}

-(void)showMessageLabel:(NSString*)message{
    UILabel *label = [UILabel new];
    label.text = message;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.frame = CGRectMake(0, 0, 280, 50);
    [label sizeToFit];
    label.textColor = [UIColor whiteColor];
    UIView *viewLabel = [UIView new];
    viewLabel.frame = CGRectMake(0, 0, CGRectGetWidth(label.frame) + 40, CGRectGetHeight(label.frame));
    viewLabel.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.view.frame) * 0.8);
    viewLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    viewLabel.layer.cornerRadius = 15;
    viewLabel.alpha = 0;
    label.center = CGPointMake(CGRectGetWidth(viewLabel.frame)/2, CGRectGetHeight(viewLabel.frame)/2);
    [viewLabel addSubview:label];
    [self.view addSubview:viewLabel];
    [UIView animateWithDuration:0.5 animations:^{
        viewLabel.alpha = 1;
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 animations:^{
            viewLabel.alpha = 0;
        } completion:^(BOOL finished) {
            [viewLabel removeFromSuperview];
        }];
    });
}

//-(void)showAlert:(NSString*)message{
//    UIAlertController * alert =  [UIAlertController
//                                  alertControllerWithTitle:@"Отправка результатов голосования"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Ok"
//                                                     style:UIAlertActionStyleCancel
//                                                   handler:nil];
//    [alert addAction:cancel];
//    [self presentViewController:alert animated:YES completion:nil];
//}

@end
