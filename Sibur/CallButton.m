//
//  CallButton.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/14/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "CallButton.h"

@implementation CallButton

-(void)awakeFromNib{
    [self addTarget:self action:@selector(tapSelf:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)tapSelf:(id)sender{
    [self showAlert];
}


- (UIViewController *)parentViewController {
    UIResponder *responder = self;
    while ([responder isKindOfClass:[UIView class]])
        responder = [responder nextResponder];
    return (UIViewController *)responder;
}

-(void)showAlert{
    NSString *message = [NSString stringWithFormat:@"Набрать номер %@ ?",self.titleLabel.text];
    UIAlertController * alert =  [UIAlertController
                                  alertControllerWithTitle:@"Подтверждение набора"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   NSString *stringURLTel = [NSString stringWithFormat:@"tel:%@",self.titleLabel.text];
                                                   stringURLTel = [stringURLTel stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                   NSURL *urlTel = [NSURL URLWithString:stringURLTel];
                                                   
                                                   [[UIApplication sharedApplication] openURL:urlTel];
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [[self parentViewController] presentViewController:alert animated:YES completion:nil];
}


@end
