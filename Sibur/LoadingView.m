//
//  LoadingView.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "LoadingView.h"

@interface LoadingView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLoading;

@property (weak, nonatomic) IBOutlet UIView *loadingView;

@end

@implementation LoadingView

-(void)setProcent:(float)procent{
    self.widthLoading.constant = CGRectGetWidth(_loadingView.frame) * procent/100;
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    }];
}

@end
