//
//  BaseOverlapView.h
//  Torless
//
//  Created by Max Vasilevsky on 8/18/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+MakeFromXib.h"

@class BaseOverlapView;

typedef void(^OverlapViewBlock)(BaseOverlapView* overlapView);

@interface BaseOverlapView : UIView

@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonApply;
@property (weak, nonatomic) IBOutlet UIView *contentView;



- (IBAction)buttonCancelAction:(id)sender;
- (IBAction)buttonApplyAction:(id)sender;


-(void)showWithApplyBlock:(OverlapViewBlock)blockApply cancelBlock:(OverlapViewBlock)blockCancel;
-(void)showWithApplyBlock:(OverlapViewBlock)blockApply;
-(void)show;
-(void)dismiss;

//-(void)showInView:(UIView*)supreview frame:(CGRect)frame applyBlock:(OverlapViewBlock)blockApply cancelBlock:(OverlapViewBlock)blockCancel;


// class methods
+(void)showWithApplyBlock:(OverlapViewBlock)blockApply cancelBlock:(OverlapViewBlock)blockCancel;
+(void)showWithApplyBlock:(OverlapViewBlock)blockApply;


+(void)show;

@end
