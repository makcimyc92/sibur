//
//  AddresButton.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "AddresButton.h"
#import <MapKit/MapKit.h>

@implementation AddresButton


-(void)awakeFromNib{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSelf:)];
    [self addGestureRecognizer:tap];
}


-(void)tapOnSelf:(id)sender{
    NSString *addressStr = self.text;
    
    if (![self.text containsString:@"Нижний Новгород"]) {
        addressStr = [NSString stringWithFormat:@"%@, Нижний Новгород",self.text];
    }
//    addressStr = [addressStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    CLLocationCoordinate2D endingCoord = [self geoCodeUsingAddress:addressStr];
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
//    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
//    [launchOptions setObject:MKLaunchOptionsMapTypeKey forKey:MKLaunchOptionsDirectionsModeKey];
    [endingItem setName:self.text];
    [endingItem openInMapsWithLaunchOptions:nil];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/maps?q=%@",  addressStr]]];

}

- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

@end
