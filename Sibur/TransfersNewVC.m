//
//  TransfersNewVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "TransfersNewVC.h"
#import "MaterialCell.h"
#import "TransfersVC.h"

@interface TransfersNewVC ()

@end

@implementation TransfersNewVC{
    NSArray *transfers;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Трансферы";
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *transfersTemp = [userDefaults objectForKey:@"transfers"];
    transfers = transfersTemp;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return transfers.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"MaterialCell";
    MaterialCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:ident bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    NSDictionary *transfer = transfers[indexPath.row];
    cell.titleDoc.text = transfer[@"title"];
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSDictionary *transfer = transfers[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"transfer%d.pdf",[transfer[@"id"] intValue]];
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (!fileExists) {
        for (NSDictionary *transferTemp in transfers) {
            if (transfer[@"url"] == transferTemp[@"url"]) {
                name = [NSString stringWithFormat:@"doc%d.pdf",[transferTemp[@"id"] intValue]];
                filePath = [basePath stringByAppendingPathComponent:name];
                fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                break;
            }
        }
    }
    if (fileExists) {
        TransfersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TransfersVC"];
        vc.navigationItem.title = transfer[@"title"];
        vc.nameFile = name;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
