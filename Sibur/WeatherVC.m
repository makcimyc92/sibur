//
//  WeatherVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 8/22/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "WeatherVC.h"
#import <MapKit/MapKit.h>
#import "HMSegmentedControl.h"

@interface WeatherVC ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightWebView;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *segment;

@end

@implementation WeatherVC{
    NSArray *weatherData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    weatherData = [[NSUserDefaults standardUserDefaults] objectForKey:@"weather"];
    // Do any additional setup after loading the view.
    [self.segmentedControl removeAllSegments];
    NSMutableArray *titlesControl = [NSMutableArray new];
    for (NSDictionary *dict in weatherData) {
        [titlesControl addObject:dict[@"title"]];
//        [self.segmentedControl insertSegmentWithTitle:dict[@"title"]
//                                              atIndex:[weatherData indexOfObject:dict]
//                                             animated:YES];
    }
//    self.segmentedControl.selectedSegmentIndex = 0;
    self.webView.scrollView.scrollEnabled = NO;
    if (weatherData.count) {
        NSString *urlString = weatherData[self.segment.selectedSegmentIndex][@"weather"];
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        [self setRegion];
    }
    
    self.segment.sectionTitles = titlesControl;
    [self.segment addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectionIndicatorColor = [UIColor colorWithRed:238.f/255.f green:115.f/255.f blue:42.f/255.f alpha:1];
    UIColor *textColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.segment.titleTextAttributes = @{NSForegroundColorAttributeName:textColor};
    self.segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    // Do any additional setup after loading the view.
}

-(void)changeValue:(HMSegmentedControl*)sender{
    NSString *urlString = weatherData[self.segment.selectedSegmentIndex][@"weather"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    [self setRegion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)changeValueSelectedControl:(id)sender{
    NSString *urlString = weatherData[self.segmentedControl.selectedSegmentIndex][@"weather"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    [self setRegion];
}

-(void)setRegion{
    NSArray<NSString*> *coord = [weatherData[self.segment.selectedSegmentIndex][@"coords"] componentsSeparatedByString:@","];
    if (coord.count < 2) {
        return;
    }
    float latitude = [[coord.firstObject stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
    float longitude = [[coord.lastObject stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
    MKCoordinateRegion region;
    region.center.latitude = latitude;
    region.center.longitude = longitude;
    region.span.latitudeDelta = 1;
    region.span.longitudeDelta = 1;
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:TRUE];
    
    
    
    
//    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake(latitude, longitude);
//    [self.mapView setCenterCoordinate:startCoord animated:YES];
//    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 8000, 8000)];
//    [self.mapView setRegion:adjustedRegion animated:YES];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.heightWebView.constant = webView.scrollView.contentSize.height;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    });
}

@end
