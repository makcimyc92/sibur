//
//  AtractionsCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/5/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "AtractionsCell.h"

@interface AtractionsCell ()

@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneHeight;

@end

@implementation AtractionsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPhoneString:(NSString *)phoneString{
    if (phoneString.length < 1) {
        self.phoneHeight.constant = 0;
    }
    self.phone.text = phoneString;
}


@end
