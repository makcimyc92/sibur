//
//  LoadingView.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic) float procent;


@end
