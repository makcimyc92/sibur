//
//  MemberCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "MemberCell.h"

@implementation MemberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.fotoView.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)layoutSubviews{
    [super layoutSubviews];
    self.fotoView.layer.cornerRadius = self.fotoView.frame.size.width/2;
}

@end
