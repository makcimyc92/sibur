//
//  Constants.h
//
//  Created by Konstantin on 12/18/15.
//  Copyright © 2015 Konstantin. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#import <UIKit/UIKit.h>

#pragma mark - Shop

extern NSString *const keyShopName;
extern NSString *const keyShopPhone;
extern NSString *const keyShopLogo;
extern NSString *const keyShopAboutLong;
extern NSString *const keyShopAboutShort;
extern NSString *const keyShopAddress;
extern NSString *const keyShopImages;
extern NSString *const keyShopLatitude;
extern NSString *const keyShopLongitude;

#pragma mark - Bike

extern NSString *const keyBikeName;
extern NSString *const keyBikeImage;
extern NSString *const keyBikeAboutShort;
extern NSString *const keyBikeAboutLong;
extern NSString *const keyBikeSpeed;
extern NSString *const keyBikeBattery;
extern NSString *const keyBikeWeight;
extern NSString *const keyBikeEngine;
extern NSString *const keyBikeStores;


#pragma mark - user Defaults

extern NSString *const keyUserDefaultsUser;
extern NSString *const keyUserDefaultsUserID;
extern NSString *const keyUserDefaultsUserMail;
extern NSString *const keyUserDefaultsUserName;
extern NSString *const keyUserDefaultsUserLastname;
extern NSString *const keyUserDefaultsUserPhone;
extern NSString *const keyUserDefaultsUserRegastratioNDate;
extern NSString *const keyUserDefaultsUserFacebookID;
extern NSString *const keyUserDefaultsUserStatus;
extern NSString *const keyUserDefaultsUserIsTracker;
extern NSString *const keyUserDefaultsUserIsInsurance;
extern NSString *const keyUserDefaultsUserIsBikeInsurance;
extern NSString *const keyUserDefaultsUserIsIsSetPanik;

extern NSString *const keyUserDefaultsShops;
extern NSString *const keyUserDefaultsBikes;
extern NSString *const keyUserDefaultsDateSetup;


#endif /* Constants_h */
