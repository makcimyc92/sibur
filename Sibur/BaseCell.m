//
//  BaseCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/14/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "BaseCell.h"

@interface BaseCell ()

@property (weak, nonatomic) IBOutlet UIButton *phone;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
@property (weak, nonatomic) IBOutlet UILabel *sheduleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adressHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sheduleHeight;


@end

@implementation BaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    [self.image addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setPhoneString:(NSString *)phoneString{
    if (phoneString.length < 1) {
        [self.phone setTitle:@"" forState:UIControlStateNormal];
        self.phoneHeight.constant = 0;
        self.phone.hidden = YES;
        return;
    }
    self.phone.hidden = NO;
    self.phoneHeight.constant = 24;
    [self.phone setTitle:phoneString forState:UIControlStateNormal];
}

-(void)setSheduleString:(NSString *)sheduleString{
    if (sheduleString.length < 1) {
        self.sheduleLabel.text = @"";
        self.sheduleHeight.constant = 0;
        return;
    }
    self.sheduleHeight.constant = 24;
    self.sheduleLabel.text = sheduleString;
}

-(void)setAdressString:(NSString *)adressString{
    if (adressString.length < 1) {
        self.adressLabel.text = @"";
        self.adressHeight.constant = 0;
        return;
    }
    self.adressHeight.constant = 24;
    self.adressLabel.text = adressString;
}




@end
