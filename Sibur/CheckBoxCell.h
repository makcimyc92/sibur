//
//  CheckBoxCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/7/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckBoxCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *variantLabel;

@property (weak, nonatomic) IBOutlet UIImageView *checkBox;

//@property (weak, nonatomic) NSMutableArray *completeVote;

//@property (weak, nonatomic) NSDictionary *completeVote;

@property (weak, nonatomic) NSMutableDictionary *completeVote;


@property (weak, nonatomic) UITableView *tableView;

@property (nonatomic) int idAnswer;

@end
