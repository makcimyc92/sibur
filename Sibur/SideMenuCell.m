//
//  SideMenuCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 8/8/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "SideMenuCell.h"

@implementation SideMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
