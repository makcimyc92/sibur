//
//  ProgrammVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "ProgrammVC.h"
#import "ProgrammCell.h"
#import "HMSegmentedControl.h"

@interface ProgrammVC () <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (weak, nonatomic) IBOutlet HMSegmentedControl *segment;

@end

@implementation ProgrammVC{
    NSArray *_events;
    NSArray *_programmDocs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *events = [userDefaults objectForKey:@"eventprogramm"];
    _programmDocs = [userDefaults objectForKey:@"docs_programm"];
    
    
//    [self.segmentedControl removeAllSegments];
    [self parseEvents:events];
    self.tableView.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    // Do any additional setup after loading the view.
//    self.segmentedControl.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.segmentedControl.layer.cornerRadius = 0.0;
//    self.segmentedControl.layer.borderWidth = 1.5f;
//    self.segmentedControl.selectedSegmentIndex = 0;
    // Do any additional setup after loading the view.
}

-(void)changeValue:(HMSegmentedControl*)sender{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)parseEvents:(NSArray*)events{
    NSMutableArray *titles = [NSMutableArray new];
    for (NSDictionary *event in events) {
        if (![titles containsObject:event[@"date"]]) {
            [titles addObject:event[@"date"]];
//            [self.segmentedControl insertSegmentWithTitle:event[@"date"]
//                                                  atIndex:[events indexOfObject:event]
//                                                 animated:YES];
        }
    }
    NSMutableArray *eventData = [NSMutableArray new];
    for (NSString *title in titles) {
        NSMutableArray *infos = [NSMutableArray new];
        for (NSDictionary *dict in events) {
            if ([title isEqualToString:dict[@"date"]]) {
                [infos addObject:dict];
            }
        }
        [eventData addObject:infos];
    }
    self.segment.sectionTitles = titles;
    [self.segment addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectionIndicatorColor = [UIColor colorWithRed:238.f/255.f green:115.f/255.f blue:42.f/255.f alpha:1];
    UIColor *textColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.segment.titleTextAttributes = @{NSForegroundColorAttributeName:textColor};
    self.segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _events = eventData;
}

#pragma mark - Action's

-(IBAction)changeValueSelectedControl:(id)sender{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array = _events[self.segment.selectedSegmentIndex];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"programmCell";
    ProgrammCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"ProgrammCell" bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    NSArray *ar = _events[self.segment.selectedSegmentIndex];
    NSDictionary *event = ar[indexPath.row];
    NSString *eventID = event[@"id"];
    NSMutableArray *docs = [NSMutableArray new];
    for (NSDictionary *doc in _programmDocs) {
        if (doc[@"program_item"] == eventID) {
            [docs addObject:doc[@"doc_id"]];
        }
    }
    cell.parent = self;
    cell.docs = docs;
    cell.eventName.text = event[@"event"];
    cell.timeLabel.text = event[@"schedule"];
    cell.phoneString = event[@"account"];
    cell.eventSubscribe.text = event[@"description"];
    if (cell.commentators.text.length < 3) {
        cell.microphoneView.hidden = YES;
    } else {
        cell.microphoneView.hidden = NO;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}




@end
