//
//  TaskIdentifier.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/7/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskIdentifier : NSURLSessionDownloadTask

@property (strong, nonatomic) NSString *identifierSting;

@end
