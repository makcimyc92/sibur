//
//  NonActionTextField.m
//  UltraLux
//
//  Created by Max Vasilevsky on 2/4/16.
//  Copyright © 2016 Makc. All rights reserved.
//

#import "NonActionTextField.h"

@implementation NonActionTextField

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    return NO;
}

- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    return CGRectZero;
}

-(void)addGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    //Prevent zooming but not panning
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
    {
        gestureRecognizer.enabled = NO;
    }
    [super addGestureRecognizer:gestureRecognizer];
    return;
}

@end
