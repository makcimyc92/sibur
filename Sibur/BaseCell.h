//
//  BaseCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/14/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *site;
//@property (weak, nonatomic) IBOutlet UILabel *place;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImage;

@property (strong, nonatomic) NSString *phoneString;
@property (strong, nonatomic) NSString *adressString;
@property (strong, nonatomic) NSString *sheduleString;


@end
