//
//  ProgrammSelectDocView.m
//  Sibur
//
//  Created by Max Vasilevsky on 8/23/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "ProgrammSelectDocView.h"

@interface ProgrammSelectDocView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableView;

@end

@implementation ProgrammSelectDocView{
    NSArray *pdfs;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *docsTemp = [userDefaults objectForKey:@"docs"];
    pdfs = docsTemp;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    float height = 44 * self.docs.count;
    self.heightTableView.constant = height > 220 ? 220 : height;
    return self.docs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    for (NSDictionary *pdf in pdfs) {
        if (pdf[@"id"] == self.docs[indexPath.row]) {
            cell.textLabel.text = pdf[@"filename"];
            break;
        }
    }
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSDictionary *doc;
    for (NSDictionary *pdf in pdfs) {
        if (pdf[@"id"] == self.docs[indexPath.row]) {
            doc = pdf;
        }
    }

    NSString *name = [NSString stringWithFormat:@"doc%d.pdf",[doc[@"id"] intValue]];
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (!fileExists) {
        for (NSDictionary *docTemp in pdfs) {
            if (doc[@"url"] == docTemp[@"url"]) {
                name = [NSString stringWithFormat:@"doc%d.pdf",[docTemp[@"id"] intValue]];
                filePath = [basePath stringByAppendingPathComponent:name];
                fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                break;
            }
        }
    }
    if (fileExists) {
        self.fileName = doc[@"filename"];
        self.name = name;
        [self buttonApplyAction:nil];
    }
}


@end
