//
//  Constants.m
//  Scale2
//
//  Created by Konstantin on 12/18/15.
//  Copyright © 2015 Konstantin. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Shop

NSString *const keyShopName               = @"name";
NSString *const keyShopPhone              = @"phone";
NSString *const keyShopLogo               = @"logo";
NSString *const keyShopAboutLong          = @"about_long";
NSString *const keyShopAboutShort         = @"about_short";
NSString *const keyShopAddress            = @"address";
NSString *const keyShopImages             = @"images";
NSString *const keyShopLatitude           = @"lat";
NSString *const keyShopLongitude          = @"len";


#pragma mark - Bike

NSString *const keyBikeName                 = @"bike_name";
NSString *const keyBikeImage                = @"bike_image";
NSString *const keyBikeAboutShort           = @"about_short";
NSString *const keyBikeAboutLong            = @"about_long";
NSString *const keyBikeSpeed                = @"speed";
NSString *const keyBikeBattery              = @"battery";
NSString *const keyBikeWeight               = @"weight";
NSString *const keyBikeEngine               = @"engine";
NSString *const keyBikeStores               = @"stores";

#pragma mark - user Defaults

NSString *const keyUserDefaultsUser                         = @"user";
NSString *const keyUserDefaultsUserID                       = @"user_id";
NSString *const keyUserDefaultsUserMail                     = @"user_email";
NSString *const keyUserDefaultsUserName                     = @"user_name";
NSString *const keyUserDefaultsUserLastname                 = @"last_name";
NSString *const keyUserDefaultsUserPhone                    = @"user_phone";
NSString *const keyUserDefaultsUserRegastratioNDate         = @"user_registration_date";
NSString *const keyUserDefaultsUserFacebookID               = @"facebook_id";
NSString *const keyUserDefaultsUserStatus                   = @"status";
NSString *const keyUserDefaultsUserIsTracker                = @"isTracker";
NSString *const keyUserDefaultsUserIsInsurance              = @"isInsurance";
NSString *const keyUserDefaultsUserIsBikeInsurance          = @"isBikeInsurance";
NSString *const keyUserDefaultsUserIsIsSetPanik            = @"isPanik";


NSString *const keyUserDefaultsBikes                = @"bikes";
NSString *const keyUserDefaultsShops                = @"shops";
NSString *const keyUserDefaultsDateSetup            = @"setupDate";
