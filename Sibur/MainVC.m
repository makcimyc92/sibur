//
//  MainVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 6/30/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "MainVC.h"
#import "MainCollectionCell.h"
#import "UIButton+TopImage.h"
#import <MFSideMenu.h>
#import "GetData.h"
#import "BaseWithNavController.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

NSString * const smItem01 = @"Участники";
NSString * const smItem02 = @"Погода";
NSString * const smItem03 = @"Материалы";
NSString * const smItem04 = @"Полезаня информация";
NSString * const smItem05 = @"Программа";
NSString * const smItem06 = @"Контакты";
NSString * const smItem07 = @"ВЭФ";


@interface MainVC () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UILabel *events;
@property (weak, nonatomic) IBOutlet UIView *viewWithEventsTable;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

//@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *collectionButton;


@end

@implementation MainVC {
    NSArray *menuItems;
    UIRefreshControl *refreshControl;
    NSURLSession *session;
    NSString *timestamp;
    int pushIndex;
    NSArray *eventsData;
    UIView *firstVerticalSeparator;
    UIView *secondVerticalSeparator;
    UIView *firstHorizontalSeparator;
    UIView *secondHorizontalSeparator;
    UIView *thirdHorizontalSeparator;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.alwaysBounceVertical = YES;
    refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    menuItems = @[smItem01, smItem02, smItem03, smItem04, smItem05, smItem06, smItem07];
    eventsData = @[@"Название мероприятия", @"Конференция", @"Фестиваль", @"Поездка"];
    self.events.text = eventsData.firstObject;
    
//    firstVerticalSeparator = [UIView new];
//    secondVerticalSeparator = [UIView new];
//    firstHorizontalSeparator = [UIView new];
//    secondHorizontalSeparator = [UIView new];
//    thirdHorizontalSeparator = [UIView new];
//    UIColor *colorSeparator = [self averageColorOfImage:self.logoImage.image];
//    firstVerticalSeparator.backgroundColor = secondVerticalSeparator.backgroundColor = firstHorizontalSeparator.backgroundColor = secondHorizontalSeparator.backgroundColor = thirdHorizontalSeparator.backgroundColor = colorSeparator;
//    [self.collectionView addSubview:firstVerticalSeparator];
//    [self.collectionView addSubview:secondVerticalSeparator];
//    [self.collectionView addSubview:firstHorizontalSeparator];
//    [self.collectionView addSubview:secondHorizontalSeparator];
//    [self.collectionView addSubview:thirdHorizontalSeparator];
}


-(void)viewDidLayoutSubviews{
    firstVerticalSeparator.frame = CGRectMake(CGRectGetWidth(self.collectionView.frame)/3 - 5, 0, 1, self.collectionView.frame.size.height);
    secondVerticalSeparator.frame = CGRectMake(CGRectGetWidth(self.collectionView.frame)/1.5, 0, 1, self.collectionView.frame.size.height);
    firstHorizontalSeparator.frame = CGRectMake(0,
                                                CGRectGetHeight(self.collectionView.frame)/4 - 5,
                                                CGRectGetWidth(self.collectionView.frame),
                                                1);
    secondHorizontalSeparator.frame = CGRectMake(0,
                                                 CGRectGetHeight(self.collectionView.frame)/2 - 5,
                                                 CGRectGetWidth(self.collectionView.frame),
                                                 1);
    thirdHorizontalSeparator.frame = CGRectMake(0,
                                                CGRectGetHeight(self.collectionView.frame)/4 * 3 - 5,
                                                CGRectGetWidth(self.collectionView.frame),
                                                1);
}

-(void)refreshTable{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue:nil];
    
    NSURL * url = [NSURL URLWithString:@"http://siburtop.aboutthecode.ru/web/index.php?r=site%2Fgetall"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *timestampUser = [userDefaults objectForKey:@"timestamp"];
    NSString *params = @"";
    NSString *login = [[NSUserDefaults standardUserDefaults] objectForKey:@"login"];
    NSString *pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"];
    if (timestampUser.length < 1) {
        params = [NSString stringWithFormat:@"data=2012-07-05 17:01:26&login=%@&password=%@",login,pass];
    } else {
        params = [NSString stringWithFormat:@"data=%@&login=%@&password=%@",timestampUser,login,pass];
    }
    
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error == nil){
            NSError *jsonError;
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                timestamp = [[GetData sharedInstance] refreshData:jsonResponse forSession:session];
                [self checkAllTasks];
            } else {
                [refreshControl endRefreshing];
            }
        } else {
            [refreshControl endRefreshing];
        }
    }];
    
    [dataTask resume];
}

-(void)checkAllTasks{
    __block BOOL needCheck;
    [session getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks) {
        if (dataTasks.count < 1 && uploadTasks.count < 1 && downloadTasks.count < 1) {
            needCheck = NO;
            [refreshControl endRefreshing];
            [[NSUserDefaults standardUserDefaults] setObject:timestamp forKey:@"timestamp"];
            NSLog(@"COMPLETE");
        } else {
            needCheck = YES;
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (needCheck) {
            [self checkAllTasks];
        }
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return menuItems.count + 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MainCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (indexPath.row == 7) {
        cell.label.text = menuItems[indexPath.row - 1];
        NSString *imageNamed = [NSString stringWithFormat:@"menu_%ld",(indexPath.row - 1)];
        cell.imageView.image = [UIImage imageNamed:imageNamed];
        return cell;
    }
    NSString *title = menuItems[indexPath.row];
    if (indexPath.row == 6) {
        cell.imageView.image = nil;
        cell.label.text = @"";
        return cell;
    }
    if ([title componentsSeparatedByString:@" "].count == 1) {
        cell.label.numberOfLines = 1;
    }
    cell.label.text = menuItems[indexPath.row];
//    NSString *imageNamed = [NSString stringWithFormat:@"menu_%ld",(long)indexPath.row];
    NSArray *sections = [[NSUserDefaults standardUserDefaults] objectForKey:@"sections"];
    NSString *imageNamed = [NSString stringWithFormat:@"menu_%ld",indexPath.row];
    cell.imageView.image = [UIImage imageNamed:imageNamed];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collectionView.frame.size.width/3 - 20, self.collectionView.frame.size.height/4 - 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [refreshControl endRefreshing];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    
    CABasicAnimation *rotateAnimation = [CABasicAnimation animation];
    rotateAnimation.keyPath = @"transform.rotation.y";
    rotateAnimation.fromValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(1)];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(360)];
    rotateAnimation.duration = 0.5;
    rotateAnimation.removedOnCompletion = YES;
    rotateAnimation.fillMode = kCAFillModeForwards;
    rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    rotateAnimation.delegate = self;
    [cell.layer addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    pushIndex = (int)indexPath.row;

}

#pragma mark - Action's

-(IBAction)tapEvents:(UITapGestureRecognizer*)sender{
    return;
    self.viewWithEventsTable.hidden = NO;
}

-(IBAction)tapBackgroundEventsView:(UITapGestureRecognizer*)sender{
    self.viewWithEventsTable.hidden = YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return eventsData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"event" forIndexPath:indexPath];
    cell.textLabel.text = eventsData[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.events.text = eventsData[indexPath.row];
    self.viewWithEventsTable.hidden = YES;
}




#pragma mark - CAAnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    BaseWithNavController *vc;
    switch (pushIndex) {
        case 0:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"company"];
            break;
        case 1:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WeatherVC"];
            break;
        case 4:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"ProgrammVC"];
            break;
        case 3:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"PoleznayaInfoVC"];
            break;
        case 7:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"TransfersVC"];
            break;
        case 11:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"Novgorod"];
            break;
        case 5:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"Contacts"];
            break;
        case 10:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"MaterialsVC"];
            break;
        case 6:
            return;
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"FeedBackVC"];
            break;
        case 2:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"MaterialsVC"];
            break;
        case 9:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"Photos"];
            break;
        default:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"NotData"];
            break;
    }
//    NSArray *sections = [[NSUserDefaults standardUserDefaults] objectForKey:@"sections"];
//    vc.sectionInfo = sections[pushIndex];
    [self.navigationController pushViewController:vc animated:YES];
}


-(UIColor *)averageColorOfImage:(UIImage*)image{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}


@end
