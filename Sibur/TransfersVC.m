//
//  TransfersVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/5/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "TransfersVC.h"

@interface TransfersVC ()

@property (weak,nonatomic) IBOutlet UIWebView *webView;

@end

@implementation TransfersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *name = self.nameFile;
    if (name.length < 1) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *docsTemp = [userDefaults objectForKey:@"docs"];
        for (NSDictionary *doc in docsTemp) {
            if ([doc[@"key"] intValue] == 11) {
                name = [NSString stringWithFormat:@"doc%d.pdf",[doc[@"id"] intValue]];
                self.title = doc[@"filename"];
            }
        }
    }
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]]];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
