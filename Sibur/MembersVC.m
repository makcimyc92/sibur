//
//  MembersVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "MembersVC.h"
#import "MemberCell.h"

@interface MembersVC ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation MembersVC{
    NSArray *searchMembers;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    [self.searchBar setBackgroundImage:[UIImage new]];
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 2;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.members.count == 0) {
        self.tableView.separatorColor = [UIColor whiteColor];
    }
    if (self.searchBar.text.length > 0) {
        return searchMembers.count;
    } else {
        return self.members.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"memberCell";
    MemberCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"MemberCell" bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    NSDictionary *member = self.searchBar.text.length > 0 ? searchMembers[indexPath.row] : self.members[indexPath.row];
    cell.nameLabel.text = member[@"fio"];
    cell.subLabel.text = member[@"position"];
    [cell.subLabel sizeToFit];
    cell.companyLabel.text = self.companyName;
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *name = [NSString stringWithFormat:@"member%d",[member[@"id"] intValue]];
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
        cell.fotoView.image = image;
    } else {
        cell.fotoView.image = nil;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}




#pragma mark - UISearchBarDelegate

//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
//    [searchBar setShowsCancelButton:YES animated:YES];
//}
//
//- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
//    
//    [searchBar resignFirstResponder];
//    [searchBar setShowsCancelButton:NO animated:YES];
//    
//}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchBar.text.length > 0) {
//        self.tableView.separatorColor = [UIColor blackColor];
        [self generateArrayMembersFilter:searchText];
    } else {
        [self.tableView reloadData];
//        self.tableView.separatorColor = [UIColor whiteColor];
    }
}

-(void)generateArrayMembersFilter:(NSString*)filter{
    NSMutableArray *newArray = [NSMutableArray new];
    for (NSDictionary *member in self.members) {
        NSString *fio = [member[@"fio"] lowercaseString];
        filter = [filter lowercaseString];
        if ([fio rangeOfString:filter].location != NSNotFound) {
            [newArray addObject:member];
        }
        //        NSArray *fioArray = [fio componentsSeparatedByString:@" "];
        //        for (NSString *str in fioArray) {
        //            if ([str hasPrefix:filter]) {
        //                [newArray addObject:member];
        //                continue;
        //            }
        //        }
    }
    searchMembers = newArray;
    [self.tableView reloadData];
    
}

@end
