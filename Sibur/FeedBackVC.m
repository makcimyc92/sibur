//
//  FeedBackVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/8/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "FeedBackVC.h"

@interface FeedBackVC ()

@property (weak, nonatomic) IBOutlet UITextField *office;
@property (weak, nonatomic) IBOutlet UITextField *ceo;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;
            
@property (weak, nonatomic) IBOutlet UITextField *event;
@property (weak, nonatomic) IBOutlet UITextField *place;
@property (weak, nonatomic) IBOutlet UITextField *organizator;
            
@property (weak, nonatomic) IBOutlet UITextField *satisfaction;
@property (weak, nonatomic) IBOutlet UITextView *dsatisfaction;
            
@property (weak, nonatomic) IBOutlet UITextField *professionalism;
@property (weak, nonatomic) IBOutlet UITextField *politentess;
@property (weak, nonatomic) IBOutlet UITextField *literacy;
@property (weak, nonatomic) IBOutlet UITextView *skills;
@property (weak, nonatomic) IBOutlet UITextField *manager;
            
@property (weak, nonatomic) IBOutlet UITextView *proposition;



@end

@implementation FeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self changeColorBorderTextField:self.office];
    [self changeColorBorderTextField:self.ceo];
    [self changeColorBorderTextField:self.name];
    [self changeColorBorderTextField:self.phone];
    [self changeColorBorderTextField:self.email];
    [self changeColorBorderTextField:self.event];
    [self changeColorBorderTextField:self.place];
    [self changeColorBorderTextField:self.organizator];
    [self changeColorBorderTextField:self.professionalism];
    [self changeColorBorderTextField:self.politentess];
    [self changeColorBorderTextField:self.literacy];
    [self changeColorBorderTextField:self.manager];
    
    self.dsatisfaction.layer.borderColor =
    self.skills.layer.borderColor =
    self.proposition.layer.borderColor =
    [UIColor colorWithRed:19.f/255.f green:123.f/255.f blue:127.f/255.f alpha:1].CGColor;
    self.dsatisfaction.layer.borderWidth = self.skills.layer.borderWidth = self.proposition.layer.borderWidth = 1;
    // Do any additional setup after loading the view.

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeColorBorderTextField:(UITextField*)textField{
//    textField.borderStyle = UITextBorderStyleLine;
    textField.layer.borderColor = [UIColor colorWithRed:19.f/255.f green:123.f/255.f blue:127.f/255.f alpha:1].CGColor;
    textField.layer.borderWidth = 1;
    textField.layer.masksToBounds = true;
}


-(IBAction)tapKa4estvo:(UIButton*)sender{
    for (UIButton *btn in sender.superview.subviews) {
        btn.selected = NO;
    }
    sender.selected = YES;
}


-(IBAction)tapSend:(UIButton*)sender{
    sender.enabled = NO;
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue:nil];
    
    NSURL * url = [NSURL URLWithString:@"http://siburtop.aboutthecode.ru/web/index.php?r=site%2Fpostfeedback"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    NSString *office = [NSString stringWithFormat:@"office=%@",self.office.text];
    NSString *ceo = [NSString stringWithFormat:@"&ceo=%@",self.ceo.text];
    NSString *name = [NSString stringWithFormat:@"&name=%@",self.name.text];
    NSString *phone = [NSString stringWithFormat:@"&phone=%@",self.phone.text];
    NSString *email = [NSString stringWithFormat:@"&email=%@",self.email.text];
    NSString *event = [NSString stringWithFormat:@"&event=%@",self.event.text];
    NSString *place = [NSString stringWithFormat:@"&place=%@",self.place.text];
    NSString *organizator = [NSString stringWithFormat:@"&organizator=%@",self.organizator.text];
    NSString *satisfaction = [NSString stringWithFormat:@"&satisfaction="];
    NSString *dsatisfaction = [NSString stringWithFormat:@"&dsatisfaction=%@",self.dsatisfaction.text];
    NSString *professionalism = [NSString stringWithFormat:@"&professionalism=%@",self.professionalism.text];
    NSString *politentess = [NSString stringWithFormat:@"&politentess=%@",self.politentess.text];
    NSString *literacy = [NSString stringWithFormat:@"&literacy=%@",self.literacy.text];
    NSString *skills = [NSString stringWithFormat:@"&skills=%@",self.skills.text];
    NSString *manager = [NSString stringWithFormat:@"&manager=%@",self.manager.text];
    NSString *proposition = [NSString stringWithFormat:@"&proposition=%@",self.proposition.text];
    NSString *params = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",
                        office,
                        ceo,
                        name,
                        phone,
                        email,
                        event,
                        place,
                        organizator,
                        satisfaction,
                        dsatisfaction,
                        professionalism,
                        politentess,
                        literacy,
                        skills,
                        manager,
                        proposition];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString *message;
        if (!error) {
//                        message = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            message = @"Спасибо, ваши данные будут обработаны";
        } else {
            message = @"Ошибка отправления";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAlert:message];
        });
        sender.enabled = YES;
    }];
    
    [dataTask resume];
}

-(void)showAlert:(NSString*)message{
    UIAlertController * alert =  [UIAlertController
                                  alertControllerWithTitle:@"Отправка результатов голосования"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Ok"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
