//
//  LoginVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "LoginVC.h"
#import "LoadingView.h"
#import "MainVC.h"
//#import "SideMenuVC.h"
//#import <MFSideMenu.h>
#import "GetData.h"

@interface LoginVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *pass;

@property (strong, nonatomic) LoadingView *loading;


@end

@implementation LoginVC{
    NSURLSession *session;
    NSString *timestamp;
    int countTask;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    countTask = 0;
    NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
    self.login.text = [userD objectForKey:@"login"];
    self.pass.text = [userD objectForKey:@"pass"];
    // Do any additional setup after loading the view.
}

-(LoadingView *)loading{
    if (!_loading) {
        _loading = [[[NSBundle mainBundle] loadNibNamed:@"LoadingView" owner:self options:nil] objectAtIndex:0];
        [self.view addSubview:_loading];
        _loading.frame = self.view.frame;
        _loading.hidden = YES;
    }
    return _loading;
}

-(IBAction)tapSend:(id)sender{
    [self.pass resignFirstResponder];
    [self.login resignFirstResponder];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *timestampUser = [userDefaults objectForKey:@"timestamp"];
    if([self connectedToInternet] == NO && timestampUser.length > 0)
    {
        // Not connected to the internet
        [self performSelectorOnMainThread:@selector(showMainVC) withObject:nil waitUntilDone:NO];
    }
    if (self.pass.text.length < 1 || self.login.text.length < 1) {
        return;
    }
    self.loading.hidden = NO;
    [self sendHTTPGet];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.login) {
        [self.pass becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}

-(void) sendHTTPGet
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue:nil];
    
    NSURL * url = [NSURL URLWithString:@"http://siburtop.aboutthecode.ru/web/index.php?r=site%2Fgetall"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *timestampUser = [userDefaults objectForKey:@"timestamp"];
    NSString *params = @"";
    if (timestampUser.length < 1) {
        params = [NSString stringWithFormat:@"data=2012-07-05 17:01:26&login=%@&password=%@",_login.text,_pass.text];
    } else {
       params = [NSString stringWithFormat:@"data=%@&login=%@&password=%@",timestampUser,_login.text,_pass.text];
    }
    
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error == nil){
            NSError *jsonError;
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                    timestamp = [[GetData sharedInstance] refreshData:jsonResponse forSession:session];
                    [self checkAllTasks];
            } else {
                [self performSelectorOnMainThread:@selector(hiddenLoadnig) withObject:nil waitUntilDone:NO];
            }
        } else {
            [self performSelectorOnMainThread:@selector(hiddenLoadnig) withObject:nil waitUntilDone:NO];
        }
    }];
    
    [dataTask resume];
}

-(void)hiddenLoadnig{
    self.loading.hidden = YES;
}

-(void)checkAllTasks{
    __block BOOL needCheck;
    [session getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks) {
        if([self connectedToInternet] == NO)
        {
            [self performSelectorOnMainThread:@selector(hiddenLoadnig) withObject:nil waitUntilDone:NO];
            return;
        }
        if (countTask == 0) {
            countTask = (int)dataTasks.count + (int)uploadTasks.count + (int)downloadTasks.count;
        }
        int currentCount = (int)dataTasks.count + (int)uploadTasks.count + (int)downloadTasks.count;
        if (currentCount) {
            float procentLoading = 100 - (float)currentCount/(float)countTask * 100;
            [self performSelectorOnMainThread:@selector(setLoadingProcent:)
                                   withObject:[NSNumber numberWithFloat:procentLoading]
                                waitUntilDone:NO];
        } else {
            [self performSelectorOnMainThread:@selector(setLoadingProcent:) withObject:@100.f waitUntilDone:NO];
        }
        if (dataTasks.count < 1 && uploadTasks.count < 1 && downloadTasks.count < 1) {
            needCheck = NO;
            [self performSelectorOnMainThread:@selector(showMainVC) withObject:nil waitUntilDone:NO];
             NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
            [userD setObject:timestamp forKey:@"timestamp"];
            [userD setObject:_login.text forKey:@"login"];
            [userD setObject:_pass.text forKey:@"pass"];
            NSLog(@"COMPLETE");
        } else {
            needCheck = YES;
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (needCheck) {
            [self checkAllTasks];
        }
    });
}

-(void)setLoadingProcent:(NSNumber*)procent{
    _loading.procent = [procent floatValue];
}

-(void)showMainVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
    self.navigationController.viewControllers = @[vc];
}
@end
