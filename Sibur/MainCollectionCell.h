//
//  MainCollectionCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 6/30/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCollectionCell : UICollectionViewCell

@property (weak,nonatomic) IBOutlet UIImageView *imageView;
@property (weak,nonatomic) IBOutlet UILabel *label;



@end
