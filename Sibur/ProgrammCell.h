//
//  ProgrammCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgrammCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventName;
@property (weak, nonatomic) IBOutlet UILabel *eventSubscribe;
@property (weak, nonatomic) IBOutlet UILabel *commentators;
//@property (weak, nonatomic) IBOutlet UILabel *place;
@property (weak, nonatomic) IBOutlet UIButton *phone;

@property (strong, nonatomic) NSString *phoneString;

@property (weak, nonatomic) IBOutlet UIImageView *microphoneView;

@property (strong, nonatomic) NSArray *docs;

@property (weak, nonatomic) IBOutlet UIButton *materials;



@property (strong, nonatomic) UIViewController *parent;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPhone;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPlace;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCommentator;

@end
