//
//  SideMenuCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 8/8/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageMenu;
@property (weak, nonatomic) IBOutlet UILabel *titleMenu;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aspectRatioImage;

 
@end
