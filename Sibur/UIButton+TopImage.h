//
//  UIButton+TopImage.h
//  Sibur
//
//  Created by Max Vasilevsky on 6/30/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (TopImage)

- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;

@end
