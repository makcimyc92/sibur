//
//  GetData.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "GetData.h"

@implementation GetData{
    NSURLSession *session;
}

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(NSString*)refreshData:(NSDictionary*)JSON forSession:(NSURLSession*)ses{
    session = ses;
    NSString *timestamp = JSON[@"timestamp"];
    [self checkingArrayForKey:@"company" inJSON:JSON];
    [self checkingArrayForKey:@"members" inJSON:JSON];
    [self checkingArrayForKey:@"eventprogramm" inJSON:JSON];
    [self checkingArrayForKey:@"hotel" inJSON:JSON];
    [self checkingArrayForKey:@"inhotel" inJSON:JSON];
    [self checkingArrayForKey:@"nearhotel" inJSON:JSON];
    [self checkingArrayForKey:@"voteq" inJSON:JSON];
    [self checkingArrayForKey:@"votea" inJSON:JSON];
    [self checkingArrayForKey:@"attractions" inJSON:JSON];
    [self checkingArrayForKey:@"restguide" inJSON:JSON];
    [self checkingArrayForKey:@"contacts" inJSON:JSON];
    [self checkingArrayForKey:@"docs" inJSON:JSON];
    [self checkingArrayForKey:@"feedback" inJSON:JSON];
    [self checkingArrayForKey:@"transfers" inJSON:JSON];
    [self checkingArrayForKey:@"photos" inJSON:JSON];
    [self checkingArrayForKey:@"sections" inJSON:JSON];
    [self checkingArrayForKey:@"weather" inJSON:JSON];
    [self checkingArrayForKey:@"infoOne" inJSON:JSON];
    [self checkingArrayForKey:@"docs_programm" inJSON:JSON];
    
//    NSArray *companyUserSorted = [self sortedIDs:companyUser];
//    NSArray *membersUserSorted = [self sortedIDs:membersUser];
//    NSArray *contactsUserSorted = [self sortedIDs:contactsUser];
//    NSArray *programmUserSorted = [self sortedIDs:programmUser];
//    NSArray *hotelUserSorted = [self sortedIDs:hotelUser];
//    NSArray *inhotelUserSorted = [self sortedIDs:inhotelUser];
//    NSArray *nearhotelUserSorted = [self sortedIDs:nearhotelUser];
//    NSArray *attractionsUserSorted = [self sortedIDs:attractionsUser];
//    NSArray *restguideUserSorted = [self sortedIDs:restguideUser];
//    NSArray *docsUserSorted = [self sortedIDs:docsUser];
//    NSArray *transfersUserSorted = [self sortedIDs:transfersUser];
//    [userDefaults setObject:membersUserSorted forKey:@"members"];
//    [userDefaults setObject:contactsUserSorted forKey:@"contacts"];
//    [userDefaults setObject:hotelUserSorted forKey:@"hotel"];
//    [userDefaults setObject:inhotelUserSorted forKey:@"inhotel"];
//    [userDefaults setObject:attractionsUserSorted forKey:@"attractions"];
//    [userDefaults setObject:restguideUserSorted forKey:@"restguide"];
//    [userDefaults setObject:docsUserSorted forKey:@"docs"];
//    [userDefaults setObject:transfersUserSorted forKey:@"transfers"];
    return timestamp;
}

-(void)checkingArrayForKey:(NSString*)key inJSON:(NSDictionary*)JSON{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrays =  JSON[key];
    NSMutableArray *arrayUser = [NSMutableArray arrayWithArray:[userDefaults objectForKey:key]];
    for (NSDictionary *dictInArrays in arrays) {
        if ([dictInArrays[@"deleted"] intValue] == 1) {
            int dictID = [dictInArrays[@"id"] intValue];
            NSDictionary *dict = [self getDictID:dictID array:arrayUser];
            if (dict) {
                [arrayUser removeObject:dict];
            }
            continue;
        }
        if (![arrayUser containsObject:dictInArrays]) {
            NSDictionary *dict = [self getDictID:[dictInArrays[@"id"] intValue] array:arrayUser];
            if (dict) {
                NSUInteger index = [arrayUser indexOfObject:dict];
                [arrayUser replaceObjectAtIndex:index withObject:dictInArrays];
            } else {
                [arrayUser addObject:dictInArrays];
            }
        } else {
            continue;
        }
        [self chooseDownloadTaskForKey:key inDictionary:dictInArrays];
    }
    NSArray *arrayUserSorted = [self sortedIDs:arrayUser];
    [userDefaults setObject:arrayUserSorted forKey:key];
}

-(void)chooseDownloadTaskForKey:(NSString*)key inDictionary:(NSDictionary*)dict{
    NSArray *keys = @[@"members",
                      @"contacts",
                      @"docs",
                      @"hotel",
                      @"inhotel",
                      @"attractions",
                      @"restguide",
                      @"transfers",
                      @"photos",
                      @"infoOne"];
    if (![keys containsObject:key]) {
        return;
    }
    if ([key isEqualToString:@"members"]) {
        NSURL *url = [NSURL URLWithString:dict[@"photo"]];
        if (url.absoluteString.length) {
            [self downloadMemberPhoto:url];
        }
    } else if ([key isEqualToString:@"contacts"]) {
        NSURL *url = [NSURL URLWithString:dict[@"photo"]];
        if (url.absoluteString.length) {
            [self downloadContactsPhoto:url];
        }
    } else if ([key isEqualToString:@"docs"]) {
        NSURL *url = [NSURL URLWithString:dict[@"url"]];
        if (url.absoluteString.length) {
            [self downloadDocs:url];
        }
    } else if ([key isEqualToString:@"hotel"]) {
        NSURL *url = [NSURL URLWithString:dict[@"image"]];
        if (url.absoluteString.length) {
            [self downloadHotelPhoto:url];
        }
    } else if ([key isEqualToString:@"inhotel"]) {
        NSURL *url = [NSURL URLWithString:dict[@"image"]];
        if (url.absoluteString.length) {
            [self downloadInHot:url];
        }
    } else if ([key isEqualToString:@"attractions"]) {
        NSURL *url = [NSURL URLWithString:dict[@"photo"]];
        if (url.absoluteString.length) {
            [self downloadAttraction:url];
        }
    } else if ([key isEqualToString:@"restguide"]) {
        NSURL *url = [NSURL URLWithString:dict[@"photo"]];
        if (url.absoluteString.length) {
            [self downloadRestguide:url];
        }
    } else if ([key isEqualToString:@"transfers"]) {
        NSURL *url = [NSURL URLWithString:dict[@"url"]];
        if (url.absoluteString.length) {
            [self downloadTransfers:url];
        }
    } else if ([key isEqualToString:@"photos"]) {
        NSURL *url = [NSURL URLWithString:dict[@"url"]];
        if (url.absoluteString.length) {
            [self downloadPhotos:url];
        }
    } else if ([key isEqualToString:@"infoOne"]) {
        NSURL *url = [NSURL URLWithString:dict[@"image"]];
        if (url.absoluteString.length) {
            [self downloadInHot:url];
        }
    }

}

-(NSArray*)sortedIDs:(NSArray*)array{
    return [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1[@"id"] intValue]> [obj2[@"id"] intValue];
    }];
}

-(NSDictionary*)getDictID:(int)idDict array:(NSArray*)array{
    for (NSDictionary *dict in array) {
        if ([dict[@"id"] intValue] == idDict) {
            return dict;
        }
    }
    return nil;
}

-(void)downloadMemberPhoto:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"MEMBER");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *members = [userDefaults objectForKey:@"members"];
        for (NSDictionary *member in members) {
            if ([url.absoluteString isEqualToString:member[@"photo"]]) {
                name = [NSString stringWithFormat:@"member%d",[member[@"id"] intValue]];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadContactsPhoto:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"contact");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *contacts = [userDefaults objectForKey:@"contacts"];
        for (NSDictionary *contact in contacts) {
            if ([url.absoluteString isEqualToString:contact[@"photo"]]) {
                name = [NSString stringWithFormat:@"contact%d",[contact[@"id"] intValue]];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadDocs:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"docs");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *docs = [userDefaults objectForKey:@"docs"];
        for (NSDictionary *doc in docs) {
            if ([url.absoluteString isEqualToString:doc[@"url"]]) {
                name = [NSString stringWithFormat:@"doc%d.pdf",[doc[@"id"] intValue]];
                break;
            }
        }
//        NSArray *transfers = [userDefaults objectForKey:@"transfers"];
//        if ([url.absoluteString isEqualToString:transfers.firstObject[@"url"]]) {
//            name = @"transfers.pdf";
//        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadHotelPhoto:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"hotel");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *hotel = [userDefaults objectForKey:@"hotel"];
        for (NSDictionary *hot in hotel) {
            if ([url.absoluteString isEqualToString:hot[@"image"]]) {
                name = @"hotel";
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadInHot:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"inhot");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *inhotel = [userDefaults objectForKey:@"infoOne"];
        for (NSDictionary *inhot in inhotel) {
            if ([url.absoluteString isEqualToString:inhot[@"image"]]) {
                name = [NSString stringWithFormat:@"inhot%d",[inhot[@"id"] intValue]];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadAttraction:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"attraction");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *attractions = [userDefaults objectForKey:@"attractions"];
        for (NSDictionary *attraction in attractions) {
            if ([url.absoluteString isEqualToString:attraction[@"photo"]]) {
                name = [NSString stringWithFormat:@"attraction%d",[attraction[@"id"] intValue]];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadRestguide:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"retguide");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *restguide = [userDefaults objectForKey:@"restguide"];
        for (NSDictionary *restg in restguide) {
            if ([url.absoluteString isEqualToString:restg[@"photo"]]) {
                name = [NSString stringWithFormat:@"rest%d",[restg[@"id"] intValue]];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadTransfers:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"transfer");
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *transfers = [userDefaults objectForKey:@"transfers"];
        for (NSDictionary *transfer in transfers) {
            if ([url.absoluteString isEqualToString:transfer[@"url"]]) {
                name = [NSString stringWithFormat:@"transfer%d.pdf",[transfer[@"id"] intValue]];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

-(void)downloadPhotos:(NSURL*)url{
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSData *data = [NSData dataWithContentsOfURL:location];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *photos = [userDefaults objectForKey:@"photos"];
        for (NSDictionary *photo in photos) {
            if ([url.absoluteString isEqualToString:photo[@"url"]]) {
                name = [NSString stringWithFormat:@"photo%d.%@",[photo[@"id"] intValue],url.pathExtension];
                break;
            }
        }
        [data writeToFile:[basePath stringByAppendingPathComponent:name] atomically:YES];
    }];
    [task resume];
}

@end
