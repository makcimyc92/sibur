//
//  BaseWithNavController.m
//  UltraLux
//
//  Created by Makc on 28.01.16.
//  Copyright © 2016 Makc. All rights reserved.
//

#import "BaseWithNavController.h"
#import "MFSideMenu.h"

//IB_DESIGNABLE

@interface BaseWithNavController ()

//@property (nonatomic) IBInspectable NSString *titleVC;
//@property (nonatomic) IBInspectable NSString *subTitleVC;
//
@property (nonatomic) IBInspectable BOOL hideRightMenu;

@end


@implementation BaseWithNavController{
    UIButton *descriptionButton;
}

- (void)viewDidLoad {
    [self getSectionInfoFromParent];
    [self setStatusBarBackgroundColor:[UIColor whiteColor]];
//    UIColor *color = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
////    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    [self.navigationController.navigationBar
//     setTitleTextAttributes:@{NSForegroundColorAttributeName : color,
//                              NSFontAttributeName : [UIFont systemFontOfSize:13]}];
//    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    if (!_hideRightMenu) {
        UIImage *menuImage = [UIImage imageNamed:@"menu"];
        menuImage = [menuImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithImage:menuImage
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(actionShowMenu:)];
        self.navigationItem.rightBarButtonItem = menuBarButton;
        if (self.navigationItem.title.length < 1) {
           self.navigationItem.title = self.sectionInfo[@"title"];
        }
        
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

        
        
//        UIImage *backImage = [UIImage imageNamed:@"back"];
////        backImage = [backImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        self.navigationController.navigationBar.backIndicatorImage = backImage;
//        self.navigationController.navigationBar.backIndicatorTransitionMaskImage = backImage;
        
//        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleBordered target:nil action:@selector(methodName)];
//        self.navigationItem.leftBarButtonItem = backButtonItem;
        descriptionButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [descriptionButton setTitle:@"Описание раздела" forState:UIControlStateNormal];
        [descriptionButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        descriptionButton.backgroundColor = [UIColor whiteColor];
        [descriptionButton addTarget:self action:@selector(alertShowDescriprion) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:descriptionButton];
        descriptionButton.hidden = YES;

    }
}


-(void)viewDidLayoutSubviews{
    descriptionButton.frame = CGRectMake(0,
                                         CGRectGetHeight(self.view.frame) - 20,
                                         CGRectGetWidth(self.view.frame),
                                         20);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}



-(void)alertShowDescriprion{
    NSString *title = self.sectionInfo[@"title"];
    NSString *message = self.sectionInfo[@"description"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)getSectionInfoFromParent{
    if (self.sectionInfo) {
        return;
    }
    int countVC = (int)self.navigationController.viewControllers.count;
    if (countVC > 1) {
        BaseWithNavController *vc = self.navigationController.viewControllers[countVC - 2];
        if (vc.sectionInfo) {
            self.sectionInfo = vc.sectionInfo;
        }
    }
}

- (void)actionShowMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

@end
