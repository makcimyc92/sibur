//
//  InfoHotelVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/6/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "InfoHotelVC.h"
//#import "InfoHotelCell.h"
//#import "NearHotelCell.h"
//#import "InHotelCell.h"
#import "BaseCell.h"

@interface InfoHotelVC () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constaintHeightWebView;

@end

@implementation InfoHotelVC{
    NSArray *hotel;
    NSArray *nearHotel;
    NSArray *inHotel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.segmentedControl.layer.borderColor= [UIColor lightGrayColor].CGColor;
    self.segmentedControl.layer.cornerRadius = 0.0;
    self.segmentedControl.layer.borderWidth = 1.5f;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://si.kitup.net/weather.html"]]];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *hotelTemp = [userDefaults objectForKey:@"hotel"];
    hotel = hotelTemp;
    NSArray *nearHotelTemp = [userDefaults objectForKey:@"nearhotel"];
    nearHotel = nearHotelTemp;
    nearHotel = [nearHotel sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return obj1[@"id"] > obj2[@"id"];
    }];
    NSArray *inHotelTemp = [userDefaults objectForKey:@"inhotel"];
    inHotel = inHotelTemp;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)changeValueSelectedControl:(UISegmentedControl*)sender{
    [self.tableView reloadData];
    self.tableView.tableHeaderView = sender.selectedSegmentIndex == 0 ? self.webView : nil;
    [self.tableView layoutIfNeeded];
    [self.tableView setNeedsLayout];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            return hotel.count;
            break;
        case 2:
            return nearHotel.count;
            break;
        case 1:
            return inHotel.count;
            break;
            
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *ident = @"BaseCell";
    NSString *name;
    BaseCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:ident bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    if (_segmentedControl.selectedSegmentIndex == 0) {
        NSDictionary *hot = hotel[indexPath.row];
        cell.nameLabel.text = hot[@"title"];
        cell.descriptionLabel.text = hot[@"description"];
        cell.adressString = hot[@"adress"];
        cell.site.text = hot[@"url"];
        cell.phoneString = hot[@"phone"];
        cell.sheduleString = @"";
        cell.nameLabel2.text = @"";
        name = @"hotel";
//        NSString *filePath = [basePath stringByAppendingPathComponent:name];
//        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
//        if (fileExists) {
//            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
//            float delta = image.size.width / (self.view.frame.size.width - 40);
//            cell.heightImage.constant = image.size.height / delta;
//            cell.image.image = image;
//        } else {
//            cell.imageSet = nil;
//        }
    } else if (_segmentedControl.selectedSegmentIndex == 2) {
        NSDictionary *nearhot = nearHotel[indexPath.row];
        cell.descriptionLabel.text = nearhot[@"description"];
        cell.adressString = nearhot[@"adress"];
        cell.sheduleString = nearhot[@"schedule"];
        cell.phoneString = nearhot[@"phone"];
        cell.nameLabel.text = nearhot[@"title"];
        cell.nameLabel2.text = @"";
        cell.site.text = @"";
    } else {
        NSDictionary *inhot = inHotel[indexPath.row];
        cell.descriptionLabel.text = inhot[@"description"];
        cell.sheduleString = inhot[@"schedule"];
        cell.phoneString = inhot[@"phone"];
        cell.nameLabel.text = inhot[@"title"];
        cell.adressString = @"";
        cell.nameLabel2.text = @"";
        cell.site.text = @"";
        name = [NSString stringWithFormat:@"inhot%d",[inhot[@"id"] intValue]];
//        NSString *filePath = [basePath stringByAppendingPathComponent:name];
//        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
//        if (fileExists) {
//            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
//            float delta = image.size.width / (self.view.frame.size.width - 40);
//            cell.heightImage.constant = image.size.height / delta;
//            cell.image.image = image;
//        } else {
//            cell.heightImage.constant = 0;
//        }
    }
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists && name.length > 0) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
        float delta = image.size.width / (self.view.frame.size.width - 40);
        cell.heightImage.constant = image.size.height / delta;
        cell.image.image = image;
    } else {
        cell.heightImage.constant = 0;
        cell.image.image = nil;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        webView.frame = CGRectMake(0, 0, self.view.frame.size.width, webView.scrollView.contentSize.height);
        self.tableView.tableHeaderView = self.segmentedControl.selectedSegmentIndex == 0 ? self.webView : nil;
        [self.tableView layoutIfNeeded];
        [self.tableView setNeedsLayout];
    });
}

@end
