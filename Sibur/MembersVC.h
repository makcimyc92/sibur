//
//  MembersVC.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "BaseWithNavController.h"

@interface MembersVC : BaseWithNavController

@property (strong, nonatomic) NSArray *members;
@property (strong, nonatomic) NSString *companyName;

@end
