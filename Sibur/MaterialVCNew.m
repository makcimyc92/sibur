//
//  MaterialVCNew.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "MaterialVCNew.h"
#import "MaterialCell.h"
#import "TransfersVC.h"
#import "HMSegmentedControl.h"

@interface MaterialVCNew ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *segment;

@end

@implementation MaterialVCNew{
//    NSArray *docs1;
//    NSArray *docs2;
    NSMutableArray *_tabsData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *docsTemp = [userDefaults objectForKey:@"docs"];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.view.backgroundColor = self.tableView.backgroundColor;
//    NSMutableArray *docs1Temp = [NSMutableArray new];
//    NSMutableArray *docs2Temp = [NSMutableArray new];
//    for (NSDictionary *doc in docsTemp) {
//        if ([doc[@"tab"] intValue] == 1) {
//            [docs1Temp addObject:doc];
//        } else {
//            [docs2Temp addObject:doc];
//        }
//    }
//    docs1 = docs1Temp;
//    docs2 = docs2Temp;
    
//    [self.segmentedControl removeAllSegments];
    NSMutableArray *arrayTitles = [NSMutableArray new];
    for (NSDictionary *doc in docsTemp) {
        NSString *title = doc[@"title"];
        if (![arrayTitles containsObject:title] && [doc[@"key"] intValue] == 11) {
            [arrayTitles addObject:title];
//            [self.segmentedControl insertSegmentWithTitle:title atIndex:[arrayTitles indexOfObject:title] animated:YES];
        }
    }
    _tabsData = [NSMutableArray new];
    for (NSString *title in arrayTitles) {
        NSMutableArray *tab = [NSMutableArray new];
        for (NSDictionary *doc in docsTemp) {
            NSString *titleDoc = doc[@"title"];
            if ([titleDoc isEqualToString:title] && [doc[@"key"] intValue] == 11) {
                [tab addObject:doc];
            }
        }
        [_tabsData addObject:tab];
    }
    
    self.segment.sectionTitles = arrayTitles;
    [self.segment addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectionIndicatorColor = [UIColor colorWithRed:238.f/255.f green:115.f/255.f blue:42.f/255.f alpha:1];
    UIColor *textColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.segment.titleTextAttributes = @{NSForegroundColorAttributeName:textColor};
    self.segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    // Do any additional setup after loading the view.
}

-(void)changeValue:(HMSegmentedControl*)sender{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}

//-(void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//    [self.tableView reloadData];
//}

//-(IBAction)changeValueSelectedControl:(id)sender{
//    [self.tableView reloadData];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self.tableView setContentOffset:CGPointZero];
//    });
//}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.segment.selectedSegmentIndex > -1 && self.segment.selectedSegmentIndex < _tabsData.count) {
        NSArray *array = _tabsData[self.segment.selectedSegmentIndex];
        return array.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"MaterialCell";
    MaterialCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:ident bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    NSArray *array = _tabsData[self.segment.selectedSegmentIndex];
    NSDictionary *doc = array[indexPath.row];
    cell.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    cell.titleDoc.text = doc[@"filename"];
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSArray *array = _tabsData[self.segment.selectedSegmentIndex];
    NSDictionary *doc = array[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"doc%d.pdf",[doc[@"id"] intValue]];
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (!fileExists) {
//        NSMutableArray *docs = [NSMutableArray arrayWithArray:docs1];
//        [docs1 arrayByAddingObjectsFromArray:docs2];
        for (NSArray *arrayDoc in _tabsData) {
            for (NSDictionary *docTemp in arrayDoc) {
                if (doc[@"url"] == docTemp[@"url"]) {
                    name = [NSString stringWithFormat:@"doc%d.pdf",[docTemp[@"id"] intValue]];
                    filePath = [basePath stringByAppendingPathComponent:name];
                    fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    break;
                }
            }
            if (fileExists) {
                break;
            }
        }
    }
    if (fileExists) {
        TransfersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TransfersVC"];
        vc.navigationItem.title = doc[@"filename"];
        vc.nameFile = name;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
