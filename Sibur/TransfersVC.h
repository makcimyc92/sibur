//
//  TransfersVC.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/5/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "BaseWithNavController.h"

@interface TransfersVC : BaseWithNavController

@property (strong, nonatomic) NSString *nameFile;

@end
