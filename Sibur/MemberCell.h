//
//  MemberCell.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemberCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *fotoView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;


@end
