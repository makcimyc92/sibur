//
//  NovgorodVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/5/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "NovgorodVC.h"
//#import "AtractionsCell.h"
//#import "RestorauntGidCell.h"
#import "BaseCell.h"

@interface NovgorodVC ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation NovgorodVC{
    NSArray *restguide;
    NSArray *attractions;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *attractionsTemp = [userDefaults objectForKey:@"attractions"];
    attractions = attractionsTemp;
    NSArray *restguideTemp = [userDefaults objectForKey:@"restguide"];
    restguide = restguideTemp;

    self.segmentedControl.layer.borderColor= [UIColor lightGrayColor].CGColor;
    self.segmentedControl.layer.cornerRadius = 0.0;
    self.segmentedControl.layer.borderWidth = 1.5f;
    // Do any additional setup after loading the view.
}

-(IBAction)changeValueSelectedControl:(id)sender{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.segmentedControl.selectedSegmentIndex == 0 ? attractions.count : restguide.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *ident = @"BaseCell";
    NSString *name;
    BaseCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:ident bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    if (_segmentedControl.selectedSegmentIndex == 0) {
        NSDictionary *attraction = attractions[indexPath.row];
        cell.nameLabel.text = attraction[@"title"];
        cell.descriptionLabel.text = attraction[@"description"];
        cell.adressString = attraction[@"adress"];
        cell.sheduleString = attraction[@"shedule"];
        cell.phoneString = @"";
        name = [NSString stringWithFormat:@"attraction%d",[attraction[@"id"] intValue]];
    } else {
        NSDictionary *restGid = restguide[indexPath.row];
        cell.nameLabel.text = restGid[@"title"];
        cell.descriptionLabel.text = restGid[@"description"];
        cell.adressString = restGid[@"adress"];
        cell.sheduleString = restGid[@"shedule"];
        cell.phoneString = restGid[@"phone"];
        name = [NSString stringWithFormat:@"rest%d",[restGid[@"id"] intValue]];
    }
    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists && name.length > 0) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
        float delta = image.size.width / (self.view.frame.size.width - 40);
        cell.heightImage.constant = image.size.height / delta;
        cell.image.image = image;
    } else {
        cell.heightImage.constant = 0;
        cell.image.image = nil;
    }
    cell.site.text = @"";
    cell.nameLabel2.text = @"";
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}



@end
