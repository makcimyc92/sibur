//
//  ProgrammCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "ProgrammCell.h"
#import "ProgrammSelectDocView.h"
#import "TransfersVC.h"

@interface ProgrammCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneHeight;

@end

@implementation ProgrammCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDocs:(NSArray *)docs{
    if (docs.count < 1) {
        self.materials.hidden = YES;
    } else {
        self.materials.hidden = NO;
    }
    _docs = docs;
}

-(void)setPhoneString:(NSString *)phoneString{
    if (phoneString.length < 1) {
        [self.phone setTitle:@"" forState:UIControlStateNormal];
        self.phoneHeight.constant = 0;
        self.phone.hidden = YES;
        return;
    }
    self.phone.hidden = NO;
    self.phoneHeight.constant = 24;
    [self.phone setTitle:phoneString forState:UIControlStateNormal];
}

-(IBAction)tapMaterials:(id)sender{
    ProgrammSelectDocView *view = [ProgrammSelectDocView makeFromXib];
    view.docs = self.docs;
    [view showWithApplyBlock:^(BaseOverlapView *overlapView) {
        TransfersVC *vc = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"TransfersVC"];
        vc.navigationItem.title = view.fileName;
        vc.nameFile = view.name;
        [self.parent.navigationController pushViewController:vc animated:YES];
    }];
}

@end
