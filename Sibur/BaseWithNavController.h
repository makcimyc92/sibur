//
//  BaseWithNavController.h
//  UltraLux
//
//  Created by Makc on 28.01.16.
//  Copyright © 2016 Makc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseWithNavController : UIViewController

@property (weak, nonatomic) UIActivityIndicatorView *activitityIndicator;

@property (strong, nonatomic) NSDictionary *sectionInfo;

@end
