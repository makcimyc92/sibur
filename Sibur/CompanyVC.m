//
//  CompanyVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/1/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "CompanyVC.h"
#import "MemberCell.h"
#import "MembersVC.h"

@interface CompanyVC () <UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSArray *companyData;
@property (strong, nonatomic) NSArray *membersData;

@end

@implementation CompanyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.searchBar setBackgroundImage:[UIImage new]];
    self.tableView.tableFooterView = [UIView new];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *company = [userDefaults objectForKey:@"company"];
    self.companyData = company;
    NSArray *members = [userDefaults objectForKey:@"members"];
    self.membersData = members;
    self.tableView.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setMembersData:(NSArray *)membersData{
    _membersData = membersData;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.searchBar.text.length > 0) {
        return self.membersData.count;
    } else {
        return self.companyData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.searchBar.text.length > 0) {
        NSString *ident = @"memberCell";
        MemberCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
        if (!cell) {
            [tableView registerNib:[UINib nibWithNibName:@"MemberCell" bundle:nil] forCellReuseIdentifier:ident];
            cell = [tableView dequeueReusableCellWithIdentifier:ident];
        }
        NSDictionary *member = self.membersData[indexPath.row];
        cell.nameLabel.text = member[@"fio"];
        cell.subLabel.text = member[@"position"];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString *name = [NSString stringWithFormat:@"member%d",[member[@"id"] intValue]];
        NSString *filePath = [basePath stringByAppendingPathComponent:name];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if (fileExists) {
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
            cell.fotoView.image = image;
        } else {
            cell.fotoView.image = nil;
        }
        int companyMemberID = [member[@"company_id"] intValue];
        NSString *companyName;
        for (NSDictionary *company in self.companyData) {
            int companyID = [company[@"id"] intValue];
            if (companyID == companyMemberID) {
                companyName = company[@"name"];
                break;
            }
        }
        cell.companyLabel.text = companyName;
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"companyCell"];
        cell.backgroundColor = [UIColor colorWithRed:45.f/255.f green:159.f/255.f blue:166.f/255.f alpha:1];
        cell.textLabel.text = self.companyData[indexPath.row][@"name"];
        return cell;
    }
}



#pragma mark - UITableViewDelegate

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.searchBar.text.length > 0) {
//        return 100;
//    }
//    return self.tableView.rowHeight;
//}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    int companyID = [self.companyData[indexPath.row][@"id"] intValue];
    NSMutableArray *membersCompany = [NSMutableArray new];
    NSArray *members = [[NSUserDefaults standardUserDefaults] objectForKey:@"members"];
    for (NSDictionary *member in members) {
        int companyMemberID = [member[@"company_id"] intValue];
        if (companyMemberID == companyID) {
            [membersCompany addObject:member];
        }
    }
    MembersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"members"];
    vc.members = membersCompany;
    vc.companyName = self.companyData[indexPath.row][@"name"];
    [self.navigationController pushViewController:vc animated:YES];
}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchBar.text.length > 0) {
        self.tableView.separatorColor = [UIColor blackColor];
        [self generateArrayMembersFilter:searchText];
    } else {
        [self.tableView reloadData];
        self.tableView.separatorColor = [UIColor whiteColor];
    }
}

-(void)generateArrayMembersFilter:(NSString*)filter{
    NSMutableArray *newArray = [NSMutableArray new];
    NSArray *members = [[NSUserDefaults standardUserDefaults] objectForKey:@"members"];
    for (NSDictionary *member in members) {
        NSString *fio = [member[@"fio"] lowercaseString];
        filter = [filter lowercaseString];
        if ([fio rangeOfString:filter].location != NSNotFound) {
           [newArray addObject:member];
        }
//        NSArray *fioArray = [fio componentsSeparatedByString:@" "];
//        for (NSString *str in fioArray) {
//            if ([str hasPrefix:filter]) {
//                [newArray addObject:member];
//                continue;
//            }
//        }
    }
    self.membersData = newArray;
}


@end
