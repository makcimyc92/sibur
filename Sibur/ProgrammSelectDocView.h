//
//  ProgrammSelectDocView.h
//  Sibur
//
//  Created by Max Vasilevsky on 8/23/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "BaseOverlapView.h"

@interface ProgrammSelectDocView : BaseOverlapView

@property (strong, nonatomic) NSArray *docs;

@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic) NSString *name;

@end
