//
//  FeedBackNewVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/14/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "FeedBackNewVC.h"

@interface FeedBackNewVC () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *recipient;
@property (weak, nonatomic) IBOutlet UITextView *message;

@end

@implementation FeedBackNewVC{
    UIPickerView *picker;
    NSArray *feedback;
    int currentID;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Обратная связь";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *feedbackTemp = [userDefaults objectForKey:@"feedback"];
    feedback = feedbackTemp;
    picker = [UIPickerView new];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    self.recipient.inputView = picker;
    
    UIToolbar *myToolbar = [[UIToolbar alloc] initWithFrame:
                            CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish)];
    //using default text field delegate method here, here you could call
    //myTextField.resignFirstResponder to dismiss the views
    [myToolbar setItems:[NSArray arrayWithObject: doneButton] animated:NO];
    self.recipient.inputAccessoryView = myToolbar;
    self.recipient.text = feedback.firstObject[@"title"];
    currentID = [feedback.firstObject[@"id"] intValue];
    
    
    [self changeColorBorderTextField:self.recipient];
    self.message.layer.cornerRadius = 5;
    self.message.layer.borderColor = [UIColor colorWithRed:19.f/255.f green:123.f/255.f blue:127.f/255.f alpha:1].CGColor;
    self.message.layer.borderWidth = 1;
    
}

-(void)changeColorBorderTextField:(UITextField*)textField{
    textField.layer.cornerRadius = 5;
    textField.layer.borderColor = [UIColor colorWithRed:19.f/255.f green:123.f/255.f blue:127.f/255.f alpha:1].CGColor;
    textField.layer.borderWidth = 1;
    textField.layer.masksToBounds = true;
}

-(void)inputAccessoryViewDidFinish{
    [self.recipient resignFirstResponder];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)tapSend:(UIButton*)sender{
    if (self.message.text.length < 1) {
        [self showMessageLabel:@"Вы не написали вопрос"];
        return;
    }
    sender.enabled = NO;
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue:nil];
    
    NSURL * url = [NSURL URLWithString:@"http://siburtop.aboutthecode.ru/web/index.php?r=site%2Fpostfeedback"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    NSString *login = [[NSUserDefaults standardUserDefaults] objectForKey:@"login"];
    NSString *params = [NSString stringWithFormat:@"deptid=%d&question=%@&user_id=%@",
                        currentID,
                        self.message.text,
                        login];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString *message;
        if (!error) {
            message = @"Спасибо предоставленная информация будет обработана!";
        } else {
            message = @"Ошибка отправления";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showMessageLabel:message];
//            [self showAlert:message];
        });
        sender.enabled = YES;
    }];
    
    [dataTask resume];
}

-(void)showMessageLabel:(NSString*)message{
    UILabel *label = [UILabel new];
    label.text = message;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.frame = CGRectMake(0, 0, 280, 50);
    [label sizeToFit];
    label.textColor = [UIColor whiteColor];
    UIView *viewLabel = [UIView new];
    viewLabel.frame = CGRectMake(0, 0, CGRectGetWidth(label.frame) + 40, CGRectGetHeight(label.frame));
    viewLabel.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.view.frame) * 0.8);
    viewLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    viewLabel.layer.cornerRadius = 15;
    viewLabel.alpha = 0;
    label.center = CGPointMake(CGRectGetWidth(viewLabel.frame)/2, CGRectGetHeight(viewLabel.frame)/2);
    [viewLabel addSubview:label];
    [self.view addSubview:viewLabel];
    [UIView animateWithDuration:0.5 animations:^{
        viewLabel.alpha = 1;
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 animations:^{
            viewLabel.alpha = 0;
        } completion:^(BOOL finished) {
            [viewLabel removeFromSuperview];
        }];
    });
}


-(void)showAlert:(NSString*)message{
    UIAlertController * alert =  [UIAlertController
                                  alertControllerWithTitle:@"Отправка результатов голосования"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Ok"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return feedback.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary *feed = feedback[row];
    return feed[@"title"];
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSDictionary *feed = feedback[row];
    self.recipient.text = feed[@"title"];
    currentID = [feed[@"id"] intValue];
}

#pragma mark - UITextViewDelegate


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
