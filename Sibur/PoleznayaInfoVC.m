//
//  PoleznayaInfoVC.m
//  Sibur
//
//  Created by Max Vasilevsky on 8/22/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "PoleznayaInfoVC.h"
#import "BaseCell.h"
#import "HMSegmentedControl.h"

@interface PoleznayaInfoVC ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *segment;



@end

@implementation PoleznayaInfoVC{
    NSArray *_infoData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    NSArray *tempInfoData = [[NSUserDefaults standardUserDefaults] objectForKey:@"infoOne"];
    [self.segmentedControl removeAllSegments];
    NSMutableArray *titlesControl = [NSMutableArray new];
    for (NSDictionary *dict in tempInfoData) {
        if (![titlesControl containsObject:dict[@"title"]]) {
            [titlesControl addObject:dict[@"title"]];
            [self.segmentedControl insertSegmentWithTitle:dict[@"title"]
                                                  atIndex:[tempInfoData indexOfObject:dict]
                                                 animated:YES];
        }
    }
    NSMutableArray *infoData = [NSMutableArray new];
    for (NSString *title in titlesControl) {
        NSMutableArray *infos = [NSMutableArray new];
        for (NSDictionary *dict in tempInfoData) {
            if ([title isEqualToString:dict[@"title"]]) {
                [infos addObject:dict];
            }
        }
        [infoData addObject:infos];
    }
    _infoData = infoData;
    self.segmentedControl.selectedSegmentIndex = 0;
    self.view.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.tableView.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    
    
    self.segment.sectionTitles = titlesControl;
    [self.segment addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectionIndicatorColor = [UIColor colorWithRed:238.f/255.f green:115.f/255.f blue:42.f/255.f alpha:1];
    UIColor *textColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    self.segment.titleTextAttributes = @{NSForegroundColorAttributeName:textColor};
    self.segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    // Do any additional setup after loading the view.
}

-(void)changeValue:(HMSegmentedControl*)sender{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)changeValueSelectedControl:(id)sender{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView setContentOffset:CGPointZero];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array = _infoData[self.segment.selectedSegmentIndex];
    return array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *ident = @"BaseCell";
    BaseCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:ident bundle:nil] forCellReuseIdentifier:ident];
        cell = [tableView dequeueReusableCellWithIdentifier:ident];
    }
    NSArray *array = _infoData[self.segment.selectedSegmentIndex];
    NSDictionary *info = array[indexPath.row];
//    cell.textLabel.numberOfLines = 0;
//    cell.textLabel.text = info[@"description"];
//    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor colorWithRed:29.f/255.f green:134.f/255.f blue:144.f/255.f alpha:1];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *name;
    cell.nameLabel.text = info[@"header"];
    cell.descriptionLabel.text = info[@"description"];
    cell.adressString = info[@"adress"];
    cell.site.text = info[@"url"];
    cell.phoneString = info[@"phone"];
    cell.sheduleString = info[@"schedule"];
    cell.nameLabel2.text = @"";
    name = [NSString stringWithFormat:@"inhot%d",[info[@"id"] intValue]];

    NSString *filePath = [basePath stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists && name.length > 0) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]];
        float delta = image.size.width / (self.view.frame.size.width - 40);
        if (image) {
            cell.heightImage.constant = image.size.height / delta;
            cell.image.image = image;
        }
    } else {
        cell.heightImage.constant = 0;
        cell.image.image = nil;
    }
    return cell;
    
    
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
