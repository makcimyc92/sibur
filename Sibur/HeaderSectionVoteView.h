//
//  HeaderSectionVoteView.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/7/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderSectionVoteView : UIView

@property (weak, nonatomic) IBOutlet UILabel *voteLabel;

@end
