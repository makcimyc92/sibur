//
//  ContactsCell.m
//  Sibur
//
//  Created by Max Vasilevsky on 7/4/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import "ContactsCell.h"

@implementation ContactsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.fotoView.layer.masksToBounds = YES;
    self.fotoView.layer.cornerRadius = 35;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
