//
//  GetData.h
//  Sibur
//
//  Created by Max Vasilevsky on 7/15/16.
//  Copyright © 2016 Max Vasilevsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetData : NSObject

+(id)sharedInstance;

-(NSString*)refreshData:(NSDictionary*)JSON forSession:(NSURLSession*)ses;

@end
