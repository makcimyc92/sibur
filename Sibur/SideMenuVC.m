//
//  SideMenuTableViewController.m
//  UltraLux
//
//  Created by Makc on 28.01.16.
//  Copyright © 2016 Makc. All rights reserved.
//

#import "SideMenuVC.h"
//#import "SideMenuTableViewCell.h"
#import "MFSideMenu.h"
#import "SideMenuCell.h"
#import "BaseWithNavController.h"
//#import "AppDelegate.h"

NSString * const mItem01 = @"Участники";
NSString * const mItem02 = @"Погода";
NSString * const mItem03 = @"Материалы";
NSString * const mItem04 = @"Полезаня информация";
NSString * const mItem05 = @"Программа";
NSString * const mItem06 = @"Контакты";
NSString * const mItem07 = @"ВЭФ";


@interface SideMenuVC () <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *menuItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SideMenuVC{
    NSURLSession *session;
    NSString *timestamp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.menuItems = @[mItem01, mItem02, mItem03, mItem04, mItem05, mItem06, mItem07];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuCell" forIndexPath:indexPath];
    cell.titleMenu.text = self.menuItems[indexPath.row];
    NSArray *sections = [[NSUserDefaults standardUserDefaults] objectForKey:@"sections"];
    NSDictionary *section = sections[indexPath.row];
    NSString *imageNamed = [NSString stringWithFormat:@"menu_%ld",indexPath.row];
    UIImage *image = [UIImage imageNamed:imageNamed];
    float aspectRatio = image.size.height/image.size.width;
//    cell.aspectRatioImage.constant = aspectRatio;
//    cell.imageMenu.image = [UIImage imageNamed:imageNamed];
    cell.imageMenu.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.imageMenu setTintColor:[UIColor lightGrayColor]];
//    cell.backgroundColor = [UIColor colorWithRed:29.f/255.f green:167.f/255.f blue:158.f/255.f alpha:1];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BaseWithNavController *vc;
    switch (indexPath.row) {
        case 0:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"company"];
            break;
        case 1:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WeatherVC"];
            break;
        case 4:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"ProgrammVC"];
            break;
        case 3:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"PoleznayaInfoVC"];
            break;
        case 6:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"TransfersVC"];
            break;
        case 11:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"Novgorod"];
            break;
        case 5:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"Contacts"];
            break;
        case 10:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"MaterialsVC"];
            break;
//        case 6:
//            return;
//            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"FeedBackVC"];
//            break;
        case 2:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"MaterialsVC"];
            break;
        case 9:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"Photos"];
            break;
        default:
            vc =[self.storyboard instantiateViewControllerWithIdentifier:@"NotData"];
            break;
    }
    UINavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"BaseNavigationController"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *main = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
    if (vc) {
//        NSArray *sections = [[NSUserDefaults standardUserDefaults] objectForKey:@"sections"];
//        vc.sectionInfo = sections[indexPath.row];
        nav.viewControllers = @[main,vc];
    }
    self.menuContainerViewController.centerViewController = nav;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

@end
